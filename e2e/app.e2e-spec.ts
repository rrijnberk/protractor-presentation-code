import { ProtractorDemoPage } from './app.po';

describe('protractor-demo App', () => {
  let page: ProtractorDemoPage;

  beforeEach(() => {
    page = new ProtractorDemoPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
